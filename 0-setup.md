# Kubernetes Overview and Client Setup

## Get Tools

Let's dump some strange things onto your machine! Start by cloning our DevOps repo:

```bash
git clone https://git.act3-ace.ai/devsecops/devops.git
# git clone git@git.act3-ace.ai:devsecops/devops.git # if you have an SSH id_rsa.pub key in Gitlab
```

Inside the repo is a script called `install_tools.sh`. Run it:

```bash
cd devops/stable/dev
./install_tools.sh
```

The script will download a bunch of tools to your `~/bin/` folder and set up autocompletion for them. You will still need to add a few lines to your `~/.bashrc` file so that your shell can actually find the tools. Follow the instructions given when the script completes.

If you don't like what the script did, you can undo it all by deleting `~/bin/`, `~/.bash_completion`, and `~/.bash_completion.d/` and removing the lines that you added to `~/.bashrc`.

The only tool we care about right now is `kubectl`. `kubectl` is similar to the `docker` tool. It's the main thing you'll use to control a cluster.

## Get your config file

Great! We have `kubectl`. It's useless right now, though, since it doesn't know about any cluster. Let's set it up to talk to our cluster.

`kubectl` expects a config file at `~/.kube/config`. The config file says how `kubectl` should connect to a cluster. There are tricks to put multiple clusters in one config file, but we're just going to use one cluster for now.

Go to the [ACT3 Rancher page](https://rancher.act3-ace.ai) (accept the insecure cert warning; I know, I know, we'll fix it eventually).

On the main screen, select Meerkat. This is our little laptop-based cluster. If you don't see it, let Jon know so he can add you to it. Click `Kubeconfig File` in the upper-right and copy the contents to `~/.kube/config` on your machine.

Now try to list resources:

```bash
kubectl get pods
```

If it says something like `No resources found in default namespace`, that's fine! It means that `kubectl` was able to talk to the cluster, and there aren't any pods in the default namespace.

## Create a project and namespace on the cluster

A *namespace* in Kubernetes is like a virtual cluster. Things in one namespace are completely separate from things in another namespace...For the most part.

You'll want your own namespace to play around in, but in order to do that, you need to make a project. Projects are a Rancher-ism (not part of Kubernetes proper); they basically just hold a bunch of namespaces and make access control easier.

In Rancher, click the `Projects/Namespaces` tab at the top. Then click `Add Project` in the upper-right. Give it a nice name, and then in the project list, find that name and click `Add Namespace`, giving it a nice name as well.

Now you have your very own namespace to play around in and break things! To use it, you'll add `-n <your-ns-name>` to your `kubectl` commands, e.g.:

```bash
kubectl -n myns get all # for the "myns" namespace
```

For the purposes of this talk, and for debugging in general, you may want to keep a terminal window open that constantly checks for resources in your namespace:

```bash
watch kubectl -n <your-ns-name> get all
```

That way you can see things being created and destroyed in realtime.

A cluster is useless without resources. Let's add something!
