# Kubernetes Lunch and Learn #1

In this first talk, you'll connect to a Kubernetes cluster, create a pod, and make it available inside and outside of the cluster!

Prerequisites:

- Basic understanding of containers
- Linux (or Mac if you're brave)
- The kind of "can-do" attitude that makes America such a wonderful place to be

Agenda:

- [Machine Setup](0-setup.md) - Install tools, get access to the cluster
- [Pods](1-pods.md) - Run containers in Kubernetes
- [Deployments](2-deployments.md) - Automate the dirty work of running pods
- [Services](3-services.md) - Make pods accessible inside the cluster
- [Ingresses](4-ingresses.md) - Make services accessible outside the cluster
