# Deployments

A *deployment* is a level above a pod. Whereas a pod is a (relatively) "tangible" thing in the cluster, a deployment is more like a standing order.

You create a deployment to tell a cluster that, not only should a certain pod be run, but it should *always* run, even if it gets stopped for some unforeseen reason.

## Create a Deployment

To make this more clear, let's create a deployment. Put the following into a file named `myfirstdeployment.yml`:

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myfirstdeployment
spec:
  selector:
    matchLabels:
      app: myfirstdeployment
  template:
    metadata:
      labels:
        app: myfirstdeployment
    spec:
      containers:
      - name: myfirstdeployment
        image: nginx
        resources:
          limits:
            memory: "128Mi"
            cpu: "500m"
        ports:
        - containerPort: 80
```

(Again, bonus points for using the VS Code *deployment* snippet.)

Just like with the pod from earlier, you can apply this file as follows:

```bash
kubectl -n <your-ns-name> apply -f myfirstdeployment.yml # tab complete!
```

Then check to see that the deployment is, indeed, in the cluster:

```bash
kubectl -n <your-ns-name> get deployments # tab tab tab tab
```

Great! Deployment is in the cluster. What does that actually mean, though?

The deployment is merely an instruction to the cluster to run some pods. It can do other fancy things, like create replicas and scale based on increased load, but we won't worry about that right now.

So if a deployment tells the cluster to run a pod, there should be a pod running right? Let's check:

```bash
kubectl -n <your-ns-name> get pods
```

And indeed there is a pod...With a really weird label at the end of its name. That's for deconfliction purposes, so your deployment can have whatever names it wants (i.e. you can name a pod something generic like `nginx` or `mongo` without worrying about other deployments in your namespace).

Now let's break some things. Try deleting the pod:

```bash
kubectl -n <your-ns-name> delete pod <tab-complete-to-get-the-name>
```

Run a `kubectl get` to see if there are any pods. Strange...You just deleted the pod, and yet there's still one there! Check the tag at the end, and you'll notice that *it's different*. What happened?

This is the magic of deployments. You created a deployment to tell the cluster that you *always* want this pod running, even if something goes wrong. And, in this case, you caused something to go wrong (by deleting the pod). Kubernetes responded by observing that the cluster didn't reflect the reality you wanted...And modified the cluster accordingly.

So that answers the issue of keeping pods running even when things go wrong, but we still can't really do anything *useful* with pods yet. All we've done is `kubectl port-forward`, and that's only supposed to be for debugging! Next we introduce the concept of Kubernetes *services*, which make your precious pods available to other pods inside the cluster.
